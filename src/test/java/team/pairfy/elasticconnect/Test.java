package team.pairfy.elasticconnect;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;
import team.pairfy.elasticconnect.domain.User;

import java.io.IOException;

public class Test {


    @org.junit.jupiter.api.Test
    void elastic_connect() {
        RestTemplate restTemplate = new RestTemplate();

        restTemplate.getInterceptors().add(
                new BasicAuthorizationInterceptor("admin", "SmAwdnBRpTcGj6ZkL91iuKTWXQOqK3BE"));

        String url = "https://yqknfz.stackhero-network.com:9200";

    }


    @org.junit.jupiter.api.Test
    void elastic_connect_post() {
        RestTemplate restTemplate = new RestTemplate();

        restTemplate.getInterceptors().add(
                new BasicAuthorizationInterceptor("admin", "SmAwdnBRpTcGj6ZkL91iuKTWXQOqK3BE"));

        String url = "https://yqknfz.stackhero-network.com:9200/inventory/_doc/1";

        // ResponseEntity<String> response = restTemplate.postForEntity(url, String.class);
    }

    @org.junit.jupiter.api.Test
    void thanks_postman_code() throws UnirestException {
        Unirest.setTimeouts(0, 0);

        HttpResponse<String> response =
                Unirest.get("https://yqknfz.stackhero-network.com:9200/")
                        .header("Authorization", "Basic YWRtaW46U21Bd2RuQlJwVGNHajZaa0w5MWl1S1RXWFFPcUszQkU=")
                        .asString();
    }

    @org.junit.jupiter.api.Test
    void post() throws UnirestException {

        // String s = new Scanner(System.in).nextLine();
        HttpResponse<String> response = Unirest
                .put("https://yqknfz.stackhero-network.com:9200/inventory/_doc/1")
                .header("Authorization", "Basic YWRtaW46U21Bd2RuQlJwVGNHajZaa0w5MWl1S1RXWFFPcUszQkU=")
                .header("Content-Type", "application/json")
                .body("{\n" +
                "    \"bugra\": \"value\"\n" +
                "}")
                .asString();
    }


    @org.junit.jupiter.api.Test
    void google() throws UnirestException {

        // String s = new Scanner(System.in).nextLine();
        HttpResponse<String> response = Unirest
                .get("https://www.google.com")
                .asString();
    }

    @org.junit.jupiter.api.Test
    void rest_high_level_client() throws IOException {
        HttpHost[] hosts = new HttpHost[1];
        hosts[0] = new HttpHost("yqknfz.stackhero-network.com", 9200, "https");


        RestClientBuilder restClientBuilder = RestClient.builder(hosts);
        Header[] defaultHeaders = new Header[1];
        defaultHeaders[0] = new BasicHeader("Authorization", "Basic YWRtaW46U21Bd2RuQlJwVGNHajZaa0w5MWl1S1RXWFFPcUszQkU");
        restClientBuilder.setDefaultHeaders(defaultHeaders);
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(restClientBuilder);

        IndexRequest indexRequest = new IndexRequest("stock");
        User user = new User();
        user.setId(1);
        user.setName("ben");
        user.setSurName("ben");

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonOfUser = objectMapper.writeValueAsString(user);
        indexRequest.id("123123").source(jsonOfUser, XContentType.JSON);

        restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
    }
}
