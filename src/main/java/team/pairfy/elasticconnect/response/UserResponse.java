package team.pairfy.elasticconnect.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import team.pairfy.elasticconnect.domain.User;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResponse {

    @JsonProperty("_source")
    User user;

    public User get_source() {
        return user;
    }

    public void set_source(User _source) {
        this.user = _source;
    }
}
