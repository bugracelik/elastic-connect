package team.pairfy.elasticconnect.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import team.pairfy.elasticconnect.domain.User;
import team.pairfy.elasticconnect.response.UserResponse;

@Repository
public class UserRepository {

    @Autowired
    ObjectMapper mapper;

    public void addUser(User user) throws UnirestException, JsonProcessingException {

        String jsonOfUser = this.mapper.writeValueAsString(user);

        HttpResponse<String> response = Unirest
                .post(String.format("https://yqknfz.stackhero-network.com:9200/inventory/_doc/%s", user.getId()))
                .header("Authorization", "Basic YWRtaW46U21Bd2RuQlJwVGNHajZaa0w5MWl1S1RXWFFPcUszQkU=")
                .header("Content-Type", "application/json")
                .body(jsonOfUser)
                .asString();
    }

    public User getUser(Integer id) throws UnirestException, JsonProcessingException {
        HttpResponse<String> response = Unirest
                .get(String.format("https://yqknfz.stackhero-network.com:9200/inventory/_doc/%s", id))
                .header("Authorization", "Basic YWRtaW46U21Bd2RuQlJwVGNHajZaa0w5MWl1S1RXWFFPcUszQkU=")
                .header("Content-Type", "application/json")
                .asString();

        // deseri
        String responseBody = response.getBody();
        UserResponse userResponse = mapper.readValue(responseBody, UserResponse.class);
        return userResponse.get_source();
    }
}

