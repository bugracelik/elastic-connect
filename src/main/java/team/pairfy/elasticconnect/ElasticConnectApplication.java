package team.pairfy.elasticconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class ElasticConnectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticConnectApplication.class, args);
    }

}

@Component // mecbur spring bean
class Bugra {

    @EventListener(ApplicationReadyEvent.class)
    public void initReady() {
        System.out.println("bugra");
    }

}