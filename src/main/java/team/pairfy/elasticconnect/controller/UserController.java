package team.pairfy.elasticconnect.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.pairfy.elasticconnect.domain.User;
import team.pairfy.elasticconnect.repository.UserRepository;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @PostMapping("")
    public String createUser(@RequestBody User user) throws JsonProcessingException, UnirestException {
        userRepository.addUser(user);
        return "elastiğe ekledik id yi siz verdiniz";
    }

    @GetMapping("id/{id}")
    public User getUser(@PathVariable Integer id) throws JsonProcessingException, UnirestException {
        return userRepository.getUser(id);
    }

    @RequestMapping("/user")
    public String loginUser(){
        return "User kullanıcı girişi başarılı";
    }

    @RequestMapping("/admin")
    public String loginAdmin(){
        return "Admin kullanıcı girişi başarılı";
    }
}


